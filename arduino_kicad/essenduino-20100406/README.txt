This is the schematics for a bare minimum arduino clone without 
serial communications. This is meant as a starting point for 
boards tested on a full arduino and then deployed using 
an atmega and few cheap components.

This is based, among other bits of information from various places, 
on the schematics for the Arduino Single-Sided Serial Board from 
http://arduino.cc/en/Main/ArduinoBoardSerialSingleSided3
licensed under Creative Commons Attribution Share-Alike 2.5 license.
http://creativecommons.org/licenses/by-sa/2.5/

