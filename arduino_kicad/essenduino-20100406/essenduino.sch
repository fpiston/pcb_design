EESchema Schematic File Version 2  date Mon 29 Mar 2010 10:11:42 CEST
LIBS:power,device,transistors,conn,linear,regul,74xx,cmos4000,adc-dac,memory,xilinx,special,microcontrollers,dsp,microchip,analog_switches,motorola,texas,intel,audio,interface,digital-audio,philips,display,cypress,siliconi,opto,atmel,contrib,valves
EELAYER 24  0
EELAYER END
$Descr A4 11700 8267
Sheet 1 1
Title ""
Date "29 mar 2010"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Kmarq B 2950 2700 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 2400 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 2500 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 2600 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 2700 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 2800 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 2900 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3300 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3400 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3500 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3600 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3700 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3800 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 3900 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4000 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4200 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4300 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4400 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4500 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4600 "Warning Pin BiDi Unconnected" F=1
Kmarq B 4850 4700 "Warning Pin BiDi Unconnected" F=1
Kmarq B 850  1800 "Warning: Pin power_out connected to Pin BiDi (net 7)" F=1
Kmarq B 1950 1300 "Warning: Pin power_out connected to Pin BiDi (net 1)" F=1
Connection ~ 2700 1350
Wire Wire Line
	2700 1350 2300 1350
Wire Wire Line
	2300 1350 2300 1600
Wire Wire Line
	1550 1600 1550 1850
Wire Wire Line
	1550 1850 1550 2000
Wire Wire Line
	1550 2000 1550 2150
Wire Wire Line
	1550 2150 1550 2400
Wire Wire Line
	1550 2400 1550 2600
Wire Wire Line
	1550 2600 1550 3100
Wire Wire Line
	1550 3100 1550 3800
Wire Wire Line
	1550 3800 1550 5000
Wire Wire Line
	2650 2150 2700 2150
Connection ~ 1550 2150
Connection ~ 2950 1300
Wire Wire Line
	2950 1300 2950 1550
Connection ~ 850  1850
Wire Wire Line
	850  1850 850  1800
Connection ~ 1000 1300
Wire Wire Line
	1000 1250 1000 1300
Wire Wire Line
	1150 1300 1100 1300
Wire Wire Line
	1100 1300 1000 1300
Wire Wire Line
	1000 1300 900  1300
Wire Wire Line
	1150 1300 1150 1350
Connection ~ 1150 1850
Connection ~ 2250 3100
Wire Wire Line
	2250 3150 2250 3100
Wire Wire Line
	2050 3800 2250 3800
Wire Wire Line
	2250 3800 2650 3800
Wire Wire Line
	2650 3800 2950 3300
Connection ~ 1550 1850
Wire Wire Line
	1950 1750 1950 1850
Wire Wire Line
	2950 3100 2250 3100
Wire Wire Line
	2250 3100 2050 3100
Wire Wire Line
	1950 1350 1950 1300
Connection ~ 1550 2400
Wire Wire Line
	1550 2400 1600 2400
Connection ~ 1550 2600
Wire Wire Line
	1550 2600 2950 2600
Wire Wire Line
	2950 2050 2950 2400
Wire Wire Line
	2950 2400 2200 2400
Wire Wire Line
	1550 5000 3850 5000
Wire Wire Line
	1550 3100 1650 3100
Connection ~ 1550 3100
Wire Wire Line
	1150 1850 1150 1750
Wire Wire Line
	1650 3800 1550 3800
Connection ~ 1550 3800
Wire Wire Line
	2250 3750 2250 3800
Connection ~ 2250 3800
Wire Wire Line
	900  1300 900  1200
Wire Wire Line
	700  1200 700  1850
Wire Wire Line
	700  1850 850  1850
Wire Wire Line
	850  1850 1000 1850
Wire Wire Line
	1000 1850 1150 1850
Wire Wire Line
	1150 1850 1550 1850
Wire Wire Line
	1550 1850 1950 1850
Wire Wire Line
	1100 1300 1100 1250
Connection ~ 1100 1300
Wire Wire Line
	1000 1850 1000 1900
Connection ~ 1000 1850
Wire Wire Line
	1950 1300 2700 1300
Wire Wire Line
	2700 1300 2950 1300
Wire Wire Line
	2950 1300 3850 1300
Wire Wire Line
	3850 1300 3850 2100
Wire Wire Line
	2950 2800 2700 2800
Wire Wire Line
	2700 2800 2700 2150
Wire Wire Line
	2700 2150 2700 2000
Connection ~ 2700 2150
Wire Wire Line
	1550 2150 2250 2150
Wire Wire Line
	1550 2000 2300 2000
Connection ~ 1550 2000
Wire Wire Line
	2700 1400 2700 1350
Wire Wire Line
	2700 1350 2700 1300
Connection ~ 2700 1300
$Comp
L C C6
U 1 1 4BB05DC2
P 2450 2150
F 0 "C6" H 2500 2250 50  0000 L CNN
F 1 "100 nF" H 2500 2050 50  0000 L CNN
	1    2450 2150
	0    1    1    0   
$EndComp
$Comp
L C C5
U 1 1 4BB05DBD
P 2300 1800
F 0 "C5" H 2350 1900 50  0000 L CNN
F 1 "100 nF" H 2350 1700 50  0000 L CNN
	1    2300 1800
	1    0    0    -1  
$EndComp
$Comp
L INDUCTOR L1
U 1 1 4BB05D86
P 2700 1700
F 0 "L1" V 2650 1700 40  0000 C CNN
F 1 "100 uH" V 2800 1700 40  0000 C CNN
	1    2700 1700
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG01
U 1 1 4BB052BE
P 850 1800
F 0 "#FLG01" H 850 2070 30  0001 C CNN
F 1 "PWR_FLAG" H 850 2030 30  0000 C CNN
	1    850  1800
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG02
U 1 1 4BB052B7
P 1100 1250
F 0 "#FLG02" H 1100 1520 30  0001 C CNN
F 1 "PWR_FLAG" H 1100 1480 30  0000 C CNN
	1    1100 1250
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 4BB0529F
P 1000 1900
F 0 "#PWR03" H 1000 1900 30  0001 C CNN
F 1 "GND" H 1000 1830 30  0001 C CNN
	1    1000 1900
	1    0    0    -1  
$EndComp
$Comp
L +8V #PWR04
U 1 1 4BB0528F
P 1000 1250
F 0 "#PWR04" H 1000 1220 20  0001 C CNN
F 1 "+8V" H 1000 1360 30  0000 C CNN
	1    1000 1250
	1    0    0    -1  
$EndComp
$Comp
L CONN_2 P1
U 1 1 4BB051DD
P 800 850
F 0 "P1" V 750 850 40  0000 C CNN
F 1 "CONN_2" V 850 850 40  0000 C CNN
	1    800  850 
	0    -1   -1   0   
$EndComp
$Comp
L CAPAPOL C1
U 1 1 4BAF5A58
P 1150 1550
F 0 "C1" H 1200 1650 50  0000 L CNN
F 1 "10 uF" H 1200 1450 50  0000 L CNN
	1    1150 1550
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 4BAF5889
P 1850 3800
F 0 "C4" H 1900 3900 50  0000 L CNN
F 1 "22 pF" H 1900 3700 50  0000 L CNN
	1    1850 3800
	0    1    1    0   
$EndComp
$Comp
L C C3
U 1 1 4BAF5885
P 1850 3100
F 0 "C3" H 1900 3200 50  0000 L CNN
F 1 "22 pF" H 1900 3000 50  0000 L CNN
	1    1850 3100
	0    1    1    0   
$EndComp
$Comp
L CRYSTAL X1
U 1 1 4BAF5861
P 2250 3450
F 0 "X1" H 2250 3600 60  0000 C CNN
F 1 "16 MHz" H 2250 3300 60  0000 C CNN
	1    2250 3450
	0    1    1    0   
$EndComp
$Comp
L CAPAPOL C2
U 1 1 4BAF55FB
P 1950 1550
F 0 "C2" H 2000 1650 50  0000 L CNN
F 1 "10 uF" H 2000 1450 50  0000 L CNN
	1    1950 1550
	1    0    0    -1  
$EndComp
$Comp
L 7805 U1
U 1 1 4BAF5572
P 1550 1300
F 0 "U1" H 1700 1060 60  0000 C CNN
F 1 "7805" H 1555 1440 60  0000 C CNN
	1    1550 1300
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW1
U 1 1 4BAF5230
P 1900 2400
F 0 "SW1" H 2050 2510 50  0000 C CNN
F 1 "SW_PUSH" H 1900 2320 50  0000 C CNN
	1    1900 2400
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 4BAF5209
P 2950 1800
F 0 "R1" V 3030 1800 50  0000 C CNN
F 1 "R10k" V 2950 1800 50  0000 C CNN
	1    2950 1800
	1    0    0    -1  
$EndComp
$Comp
L ATMEGA8-P IC1
U 1 1 4BAF4DE2
P 3850 3500
F 0 "IC1" H 3150 4750 50  0000 L BNN
F 1 "ATMEGA8-P" H 4100 2100 50  0000 L BNN
F 2 "DIL28" H 4350 2025 50  0001 C CNN
	1    3850 3500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
