<./pistuino-F_SilkS.pdf>

<table id="tab:componentes" border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
<caption class="t-above"><span class="table-number">Table 1:</span> Lista de componentes</caption>

<colgroup>
<col  class="right" />

<col  class="left" />

<col  class="left" />

<col  class="right" />

<col  class="left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="right">Id</th>
<th scope="col" class="left">Designator</th>
<th scope="col" class="left">Package</th>
<th scope="col" class="right">Quantity</th>
<th scope="col" class="left">Value</th>
</tr>
</thead>

<tbody>
<tr>
<td class="right">1</td>
<td class="left">C5</td>
<td class="left">Capacitor7.5MMDiscRM5</td>
<td class="right">1</td>
<td class="left">0.1 uF</td>
</tr>


<tr>
<td class="right">2</td>
<td class="left">C6</td>
<td class="left">Capacitor7.5MMDiscRM5</td>
<td class="right">1</td>
<td class="left">0.1 uF</td>
</tr>


<tr>
<td class="right">3</td>
<td class="left">C7,C8</td>
<td class="left">Capacitor7.5MMDiscRM5</td>
<td class="right">2</td>
<td class="left">22 pF</td>
</tr>


<tr>
<td class="right">4</td>
<td class="left">R3</td>
<td class="left">Resistor<sub>Horizontal</sub><sub>RM10mm</sub></td>
<td class="right">1</td>
<td class="left">1k</td>
</tr>


<tr>
<td class="right">5</td>
<td class="left">SW1</td>
<td class="left">SW<sub>PUSH</sub><sub>SMALL</sub></td>
<td class="right">1</td>
<td class="left">SW<sub>PUSH</sub></td>
</tr>


<tr>
<td class="right">6</td>
<td class="left">U1</td>
<td class="left">LM78XXV</td>
<td class="right">1</td>
<td class="left">7805</td>
</tr>


<tr>
<td class="right">7</td>
<td class="left">C2</td>
<td class="left">Capacitor7.5MMDiscRM5</td>
<td class="right">1</td>
<td class="left">0.1uf</td>
</tr>


<tr>
<td class="right">8</td>
<td class="left">X1</td>
<td class="left">Crystal<sub>HC49</sub>-U<sub>Vertical</sub></td>
<td class="right">1</td>
<td class="left">CRYSTAL</td>
</tr>


<tr>
<td class="right">9</td>
<td class="left">R1</td>
<td class="left">Resistor<sub>Horizontal</sub><sub>RM15mm</sub></td>
<td class="right">1</td>
<td class="left">10k</td>
</tr>


<tr>
<td class="right">10</td>
<td class="left">D4</td>
<td class="left">LED-3MM</td>
<td class="right">1</td>
<td class="left">LED</td>
</tr>


<tr>
<td class="right">11</td>
<td class="left">C4,C9</td>
<td class="left">Capacitor7.5MMDiscRM5</td>
<td class="right">2</td>
<td class="left">0.1uF</td>
</tr>


<tr>
<td class="right">12</td>
<td class="left">D3</td>
<td class="left">Diode<sub>DO</sub>-41<sub>SOD81</sub><sub>Horizontal</sub><sub>RM10</sub></td>
<td class="right">1</td>
<td class="left">DIODE</td>
</tr>


<tr>
<td class="right">13</td>
<td class="left">IC1</td>
<td class="left">DIP-28\_<sub>300</sub><sub>ELL</sub></td>
<td class="right">1</td>
<td class="left">ATMEGA328-P</td>
</tr>


<tr>
<td class="right">14</td>
<td class="left">C3,C1</td>
<td class="left">Capacitor5x6RM2.5</td>
<td class="right">2</td>
<td class="left">47 ufd</td>
</tr>


<tr>
<td class="right">15</td>
<td class="left">5V<sub>in1,USB</sub><sub>JV1,V</sub><sub>IN1</sub></td>
<td class="left">Pin<sub>Header</sub><sub>Straight</sub><sub>1x02</sub></td>
<td class="right">3</td>
<td class="left">CONN<sub>2</sub></td>
</tr>


<tr>
<td class="right">16</td>
<td class="left">ICSP1</td>
<td class="left">Pin<sub>Header</sub><sub>Straight</sub><sub>2x03</sub></td>
<td class="right">1</td>
<td class="left">CONN<sub>3X2</sub></td>
</tr>


<tr>
<td class="right">17</td>
<td class="left">P2</td>
<td class="left">Pin<sub>Header</sub><sub>Angled</sub><sub>1x06</sub></td>
<td class="right">1</td>
<td class="left">FTDI</td>
</tr>


<tr>
<td class="right">18</td>
<td class="left">SHIELD1</td>
<td class="left">ARDUINO<sub>SHIELD</sub><sub>2</sub></td>
<td class="right">1</td>
<td class="left">ARDUINO<sub>SHIELD</sub></td>
</tr>


<tr>
<td class="right">19</td>
<td class="left">P1</td>
<td class="left">TO220<sub>VERT</sub></td>
<td class="right">1</td>
<td class="left">CONN<sub>01X03</sub></td>
</tr>
</tbody>
</table>