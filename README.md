
# Introduction #

The Arduino like PCBs have been developed using the following sources:

* Serialino, a serial Arduino clone designed with Kicad.
  <http://www.hw2sw.com/2013/01/14/serialino-the-pcb/>

* Arduino Single-Sided Serial Board (version 3).
  <http://arduino.cc/en/Main/ArduinoBoardSerialSingleSided3>

* Diavolino, the Arduino clone from 'Evil Mad Scientist'.
  <http://shop.evilmadscientist.com/productsmenu/tinykitlist/180-diavolino>

All design are under Kicad enviroment.
