update=Fri 08 May 2015 08:06:36 AM CEST
last_client=kicad
[pcbnew]
version=1
PageLayoutDescrFile=
LastNetListRead=GPRSshield v1_4_.net
UseCmpFile=1
PadDrill=0
PadDrillOvalY=0
PadSizeH=1.175
PadSizeV=1.175
PcbTextSizeV=1.5
PcbTextSizeH=1.5
PcbTextThickness=0.3
ModuleTextSizeV=1
ModuleTextSizeH=1
ModuleTextSizeThickness=0.15
SolderMaskClearance=0.2
SolderMaskMinWidth=0
DrawSegmentWidth=0.2
BoardOutlineThickness=0.15
ModuleOutlineThickness=0.15
[eeschema]
version=1
LibDir=../../../../../org/code/program_config/kicad/smisioto_libraries/kicad_libs/library
[eeschema/libraries]
LibName1=NOVVO_GPRSshield_v1-1-rescue
LibName2=74xgxx
LibName3=74xx
LibName4=ac-dc
LibName5=actel
LibName6=adc-dac
LibName7=analog_switches
LibName8=atmel
LibName9=audio
LibName10=brooktre
LibName11=cmos4000
LibName12=cmos_ieee
LibName13=conn
LibName14=contrib
LibName15=cypress
LibName16=dc-dc
LibName17=device
LibName18=digital-audio
LibName19=display
LibName20=dsp
LibName21=elec-unifil
LibName22=ftdi
LibName23=gennum
LibName24=graphic
LibName25=hc11
LibName26=intel
LibName27=interface
LibName28=ir
LibName29=linear
LibName30=logo
LibName31=memory
LibName32=microchip
LibName33=microchip_pic10mcu
LibName34=microchip_pic12mcu
LibName35=microchip_pic16mcu
LibName36=microchip_pic18mcu
LibName37=microchip_pic32mcu
LibName38=microcontrollers
LibName39=motorola
LibName40=msp430
LibName41=nordicsemi
LibName42=nxp_armmcu
LibName43=opto
LibName44=philips
LibName45=power
LibName46=powerint
LibName47=pspice
LibName48=references
LibName49=regul
LibName50=relays
LibName51=rfcom
LibName52=sensors
LibName53=silabs
LibName54=siliconi
LibName55=special
LibName56=stm8
LibName57=stm32
LibName58=supertex
LibName59=texas
LibName60=transf
LibName61=transistors
LibName62=ttl_ieee
LibName63=valves
LibName64=video
LibName65=xilinx
LibName66=/media/fpistonHD2/org/code/program_config/kicad/smisioto_libraries/kicad_libs/library/w_device
LibName67=w_connectors
