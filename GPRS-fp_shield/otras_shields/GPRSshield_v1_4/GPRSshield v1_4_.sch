EESchema Schematic File Version 1
LIBS:CONNECTER,CPT_SMD,Connector,Discrete,ICs_v54,Power or GND ,SparkFun2,mySeeed,template,.\GPRSshield v1_4_-cache-cache
EELAYER 23 0
EELAYER END
$Descr A4 11700 8267
Sheet 1 1
Title "GPRSshield v1.4.sch"
Date "29 APR 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 4550 4400 0 40 ~
close to connector
Text Notes 7300 4550 0 40 ~
debug
Text Notes 8476 7426 0 100 ~
TITLE:  GPRS Shield
Text Notes 8801 7040 0 254 ~
SeeedStudio
Text Notes 8476 7751 0 70 ~
Design:
Text Notes 10051 7751 0 70 ~
Check:
Text Notes 8476 8066 0 70 ~
Data: 2012/06/24
Text Notes 10051 8066 0 70 ~
Vision: 1.4
Text Notes 10964 8072 0 70 ~
Sheet: 1/1
Text Notes 193 200 0 70 ~
power supply
Text Notes 1650 2200 0 40 ~
The power trace should be short and wide, recommended above 80mil
Text Notes 1650 2300 0 40 ~
The ground layer (top and bottom) must be connected with many vias, 
Text Notes 1650 2400 0 40 ~
especially under the module's shield case.
Text Notes 207 2600 0 70 ~
Uart voltage level shift
Text Notes 2350 3950 0 40 ~
R7=510R,0603 package,max power is 1/10.
Text Notes 2150 4050 0 40 ~
R7 disappear power =(Vin-Vz)2/R7, less than 1/10, is OK
Text Notes 196 5600 0 70 ~
RTC battery
Text Notes 186 4300 0 70 ~
SIM card holder
Text Notes 4450 317 0 70 ~
GPRS moudle
Text Notes 8675 425 0 70 ~
connector
Text Notes 6299 8053 0 70 ~
Change Note:
$Comp
L ANT_ANTENNA-SMA-IPX ANT1
U 1 1 5540CB48
P 10450 5150
F 0 "ANT1" H 10351 5350 70 0000 L B
	1    10450 5150
	1    0    0    -1  
$EndComp
$Comp
L ANT_ANTENNA-U-FL AT1
U 1 1 5540CB48
P 8150 3800
F 0 "AT1" H 8000 4000 70 0000 L B
F 1 "U-FL" H 7950 3550 70 0000 L B
	1    8150 3800
	1    0    0    -1  
$EndComp
$Comp
L C_C* C1
U 1 1 5540CB48
P 1150 950
F 0 "C1" H 1000 950 50 0000 L B
F 1 "10u" H 1150 950 50 0000 L B
	1    1150 950 
	0    -1   -1   0   
$EndComp
$Comp
L C_C* C2
U 1 1 5540CB48
P 1400 950
F 0 "C2" H 1250 950 50 0000 L B
F 1 "10u" H 1400 950 50 0000 L B
	1    1400 950 
	0    -1   -1   0   
$EndComp
$Comp
L C_006-CAP* C3
U 1 1 5540CB48
P 1650 950
F 0 "C3" H 1500 999 40 0000 L B
F 1 "470u" H 1500 900 40 0000 L B
F 0 "+" H 1550 950 50 0000 L B
F 1 "-" H 1700 950 50 0000 L B
	1    1650 950 
	0    1    1    0   
$EndComp
$Comp
L C_006-CAP* C4
U 1 1 5540CB48
P 3550 1000
F 0 "C4" H 3400 1049 40 0000 L B
F 1 "470u" H 3400 950 40 0000 L B
F 0 "+" H 3450 1000 50 0000 L B
F 1 "-" H 3600 1000 50 0000 L B
	1    3550 1000
	0    1    1    0   
$EndComp
$Comp
L C_C* C5
U 1 1 5540CB48
P 3800 1000
F 0 "C5" H 3650 1000 50 0000 L B
F 1 "100n" H 3800 1000 50 0000 L B
	1    3800 1000
	0    -1   -1   0   
$EndComp
$Comp
L C_C* C6
U 1 1 5540CB48
P 4700 3200
F 0 "C6" H 4550 3200 40 0000 L B
F 1 "4.7u" H 4700 3200 40 0000 L B
	1    4700 3200
	1    0    0    -1  
$EndComp
$Comp
L C_C* C7
U 1 1 5540CB48
P 11050 4200
F 0 "C7" H 10900 4200 40 0000 L B
F 1 "2.2u" H 11050 4200 40 0000 L B
	1    11050 4200
	1    0    0    -1  
$EndComp
$Comp
L C_C* C8
U 1 1 5540CB48
P 11050 4300
F 0 "C8" H 10900 4300 40 0000 L B
F 1 "2.2u" H 11050 4300 40 0000 L B
	1    11050 4300
	1    0    0    -1  
$EndComp
$Comp
L C_C* C9
U 1 1 5540CB48
P 3000 3350
F 0 "C9" H 2850 3350 50 0000 L B
F 1 "1uF" H 3000 3350 50 0000 L B
	1    3000 3350
	0    -1   -1   0   
$EndComp
$Comp
L C_C* C10
U 1 1 5540CB48
P 7150 4350
F 0 "C10" H 7000 4350 40 0000 L B
F 1 "DNP" H 7150 4350 50 0000 L B
	1    7150 4350
	0    -1   -1   0   
$EndComp
$Comp
L C_C* C11
U 1 1 5540CB48
P 7600 4350
F 0 "C11" H 7450 4350 40 0000 L B
F 1 "DNP" H 7600 4350 50 0000 L B
	1    7600 4350
	0    -1   -1   0   
$EndComp
$Comp
L C_C* C12
U 1 1 5540CB48
P 5050 4550
F 0 "C12" H 4900 4550 40 0000 L B
F 1 "33p" H 5050 4550 40 0000 L B
	1    5050 4550
	-1   0    0    1   
$EndComp
$Comp
L C_C* C13
U 1 1 5540CB48
P 5650 4550
F 0 "C13" H 5500 4550 40 0000 L B
F 1 "10p" H 5650 4550 40 0000 L B
	1    5650 4550
	-1   0    0    1   
$EndComp
$Comp
L C_C* C14
U 1 1 5540CB48
P 6000 4550
F 0 "C14" H 5850 4550 40 0000 L B
F 1 "10p" H 6000 4550 40 0000 L B
	1    6000 4550
	-1   0    0    1   
$EndComp
$Comp
L C_C* C15
U 1 1 5540CB48
P 6350 4550
F 0 "C15" H 6200 4550 40 0000 L B
F 1 "10p" H 6350 4550 40 0000 L B
	1    6350 4550
	-1   0    0    1   
$EndComp
$Comp
L C_C* C16
U 1 1 5540CB48
P 5050 4850
F 0 "C16" H 4900 4850 50 0000 L B
F 1 "33p" H 5050 4850 50 0000 L B
	1    5050 4850
	1    0    0    -1  
$EndComp
$Comp
L C_C* C17
U 1 1 5540CB48
P 5650 4850
F 0 "C17" H 5500 4850 40 0000 L B
F 1 "33p" H 5650 4850 40 0000 L B
	1    5650 4850
	-1   0    0    1   
$EndComp
$Comp
L C_C* C18
U 1 1 5540CB48
P 6000 4850
F 0 "C18" H 5850 4850 40 0000 L B
F 1 "33p" H 6000 4850 40 0000 L B
	1    6000 4850
	-1   0    0    1   
$EndComp
$Comp
L C_C* C19
U 1 1 5540CB48
P 6350 4850
F 0 "C19" H 6200 4850 40 0000 L B
F 1 "33p" H 6350 4850 40 0000 L B
	1    6350 4850
	-1   0    0    1   
$EndComp
$Comp
L C_C* C20
U 1 1 5540CB48
P 1650 4600
F 0 "C20" H 1500 4600 40 0000 L B
F 1 "100n" H 1650 4600 40 0000 L B
	1    1650 4600
	0    -1   -1   0   
$EndComp
$Comp
L D_D-VOL D1
U 1 1 5540CB48
P 3200 3350
F 0 "D1" H 3050 3450 70 0000 L B
F 1 "MMSZ4689-V" H 3050 3200 70 0000 L B
	1    3200 3350
	0    -1   -1   0   
$EndComp
$Comp
L D_LED* D2
U 1 1 5540CB48
P 7900 6200
F 0 "D2" H 7750 6200 40 0000 L B
F 1 "red" H 7900 6200 40 0000 L B
	1    7900 6200
	0    -1   -1   0   
$EndComp
$Comp
L D_LED* D3
U 1 1 5540CB48
P 7150 6250
F 0 "D3" H 7000 6250 40 0000 L B
F 1 "green" H 7150 6250 40 0000 L B
	1    7150 6250
	0    -1   -1   0   
$EndComp
$Comp
L J_DC-044 EXT_PWR
U 1 1 5540CB48
P 900 1700
F 0 "EXT_PWR" H 950 1650 40 0000 L B
F 1 "DC-044" H 949 1600 40 0000 L B
F 0 "+" H 550 1775 50 0000 L B
F 1 "-" H 550 1576 50 0000 L B
F 2 "CDN" H 550 1681 40 0000 L B
	1    900  1700
	1    0    0    -1  
$EndComp
$Comp
L J_HEADER-2X5P J1
U 1 1 5540CB48
P 10900 1200
F 0 "J1" H 10751 1450 40 0000 L B
F 1 "" V 10949 1000 70 0000 L B
	1    10900 1200
	1    0    0    -1  
$EndComp
$Comp
L J_HEADER_1X6 J2
U 1 1 5540CB48
P 8950 1250
F 0 "J2" H 8670 1570 40 0000 L B
F 1 "6p male dowm" H 8660 890 40 0000 L B
	1    8950 1250
	-1   0    0    1   
$EndComp
$Comp
L J_HEADER_1X6 J3
U 1 1 5540CB48
P 9700 1250
F 0 "J3" H 9420 1570 40 0000 L B
F 1 "6p female up" H 9410 890 40 0000 L B
	1    9700 1250
	1    0    0    -1  
$EndComp
$Comp
L J_CK_2X6 J4
U 1 1 5540CB48
P 10900 2000
F 0 "J4" H 10701 2300 40 0000 L B
F 1 "CK_2X6" H 10700 1600 40 0000 L B
	1    10900 2000
	1    0    0    -1  
$EndComp
$Comp
L J_HEADER_1X6 J5
U 1 1 5540CB48
P 8950 2250
F 0 "J5" H 8670 2570 40 0000 L B
F 1 "6p male dowm" H 8660 1890 40 0000 L B
	1    8950 2250
	-1   0    0    1   
$EndComp
$Comp
L J_HEADER_1X6 J6
U 1 1 5540CB48
P 9700 2250
F 0 "J6" H 9420 2570 40 0000 L B
F 1 "6p female up" H 9410 1890 40 0000 L B
	1    9700 2250
	1    0    0    -1  
$EndComp
$Comp
L J_CK_2X3 J7
U 1 1 5540CB48
P 10900 2900
F 0 "J7" H 10700 3120 40 0000 L B
F 1 "CK_2X3_DD" H 10700 2630 40 0000 L B
	1    10900 2900
	1    0    0    -1  
$EndComp
$Comp
L J_CK_1X8 J8
U 1 1 5540CB48
P 9000 3250
F 0 "J8" H 8900 3680 40 0000 L B
F 1 "8p male dowm" H 8901 2780 40 0000 L B
	1    9000 3250
	-1   0    0    1   
$EndComp
$Comp
L J_CK_1X8 J9
U 1 1 5540CB48
P 9650 3250
F 0 "J9" H 9551 3680 40 0000 L B
F 1 "8p female up" H 9550 2780 40 0000 L B
	1    9650 3250
	1    0    0    -1  
$EndComp
$Comp
L J_HEADER-4P J10
U 1 1 5540CB48
P 10450 3750
F 0 "J10" H 10300 3950 40 0000 L B
F 1 "" V 10450 3600 50 0000 L B
	1    10450 3750
	-1   0    0    -1  
$EndComp
$Comp
L J_CK_1X8 J11
U 1 1 5540CB48
P 9000 4400
F 0 "J11" H 8900 4830 40 0000 L B
F 1 "8p female up" H 8901 3930 40 0000 L B
	1    9000 4400
	-1   0    0    1   
$EndComp
$Comp
L J_CK_1X8 J12
U 1 1 5540CB48
P 9650 4400
F 0 "J12" H 9551 4830 40 0000 L B
F 1 "8p male dowm" H 9550 3930 40 0000 L B
	1    9650 4400
	1    0    0    -1  
$EndComp
$Comp
L J_HEADER_1X6 J13
U 1 1 5540CB48
P 10450 4450
F 0 "J13" H 10170 4770 40 0000 L B
F 1 "" H 10160 4090 50 0000 L B
	1    10450 4450
	-1   0    0    1   
$EndComp
$Comp
L J_HOLDER-BATTERY-2P J14
U 1 1 5540CB48
P 2050 6100
F 0 "J14" H 1750 6299 40 0000 L B
F 1 "" H 1750 5900 30 0000 L B
	1    2050 6100
	-1   0    0    1   
$EndComp
$Comp
L P_PAD-SOLDERJUMPER JP
U 1 1 5540CB48
P 5100 1050
F 0 "JP" H 5050 1150 40 0000 L B
	1    5100 1050
	0    1    1    0   
$EndComp
$Comp
L K_BOTTON_1 K1
U 1 1 5540CB48
P 4450 1900
F 0 "K1" H 4700 2100 40 0000 L B
	1    4450 1900
	0    -1   -1   0   
$EndComp
$Comp
L J_AUDIO-SMT MIC
U 1 1 5540CB48
P 4900 6400
F 0 "MIC" V 5050 6351 40 0000 L B
F 1 "" H 4150 6599 50 0000 L B
	1    4900 6400
	0    1    1    0   
$EndComp
$Comp
L JP_JP P1
U 1 1 5540CB48
P 8200 4200
F 0 "P1" H 8050 4300 40 0000 L B
F 1 "JP" H 8250 4300 40 0000 L B
	1    8200 4200
	1    0    0    -1  
$EndComp
$Comp
L P_PAD-JUMPER-2P P2
U 1 1 5540CB48
P 10950 5350
F 0 "P2" H 10850 5460 50 0000 L B
	1    10950 5350
	-1   0    0    1   
$EndComp
$Comp
L D_007-LED* PWR
U 1 1 5540CB48
P 1600 450
F 0 "PWR" H 1450 450 40 0000 L B
F 1 "green" H 1600 450 40 0000 L B
	1    1600 450 
	1    0    0    -1  
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q1
U 1 1 5540CB48
P 4700 1300
F 0 "Q1" H 4600 1349 40 0000 L B
F 1 "9013" H 4600 1250 40 0000 L B
F 0 "B" H 4675 1300 25 0000 L B
F 1 "C" H 4775 1400 25 0000 L B
F 2 "E" H 4775 1175 25 0000 L B
	1    4700 1300
	-1   0    0    1   
$EndComp
$Comp
L Q_010-DUAL-N-MOSFET* Q2
U 1 1 5540CB48
P 1900 3200
F 0 "Q2A" H 1700 3249 40 0000 L B
F 1 "Si5902BDC" H 1700 3150 40 0000 L B
F 0 "D" H 1875 3287 32 0000 L B
F 1 "S" H 1876 3075 32 0000 L B
F 2 "G" H 1762 3200 32 0000 L B
F 3 "D" H 1924 3287 32 0000 L B
	1    1900 3200
	0    1    1    0   
$EndComp
$Comp
L Q_010-DUAL-N-MOSFET* Q2
U 1 1 5540CB48
P 1900 3650
F 0 "Q2B" H 1700 3699 40 0000 L B
F 1 "Si5902BDC" H 1700 3600 40 0000 L B
F 0 "D" H 1875 3737 32 0000 L B
F 1 "S" H 1876 3525 32 0000 L B
F 2 "G" H 1762 3650 32 0000 L B
F 3 "D" H 1924 3737 32 0000 L B
	1    1900 3650
	0    1    1    0   
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q3
U 1 1 5540CB48
P 6950 5500
F 0 "Q3" H 6850 5549 40 0000 L B
F 1 "9013" H 6850 5450 40 0000 L B
F 0 "B" H 6925 5500 25 0000 L B
F 1 "C" H 7024 5600 25 0000 L B
F 2 "E" H 7025 5376 25 0000 L B
	1    6950 5500
	0    1    1    0   
$EndComp
$Comp
L Q_TRANSISTOR-NPN* Q4
U 1 1 5540CB48
P 7700 5500
F 0 "Q4" H 7600 5549 40 0000 L B
F 1 "9013" H 7600 5450 40 0000 L B
F 0 "B" H 7675 5500 25 0000 L B
F 1 "C" H 7774 5600 25 0000 L B
F 2 "E" H 7775 5376 25 0000 L B
	1    7700 5500
	0    1    1    0   
$EndComp
$Comp
L R_R* R1
U 1 1 5540CB48
P 2000 450
F 0 "R1" H 1850 450 40 0000 L B
F 1 "1k" H 2000 450 40 0000 L B
	1    2000 450 
	1    0    0    -1  
$EndComp
$Comp
L R_R* R2
U 1 1 5540CB48
P 4800 1100
F 0 "R2" H 4650 1100 40 0000 L B
F 1 "47k" H 4800 1100 40 0000 L B
	1    4800 1100
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R3
U 1 1 5540CB48
P 4950 1300
F 0 "R3" H 4800 1300 40 0000 L B
F 1 "4.7k" H 4950 1300 40 0000 L B
	1    4950 1300
	-1   0    0    1   
$EndComp
$Comp
L R_R* R4
U 1 1 5540CB48
P 3350 1000
F 0 "R4" H 3200 1000 50 0000 L B
F 1 "100k" H 3350 1000 50 0000 L B
	1    3350 1000
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R5
U 1 1 5540CB48
P 4000 1000
F 0 "R5" H 3850 1000 50 0000 L B
F 1 "470R" H 4000 1000 50 0000 L B
	1    4000 1000
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R6
U 1 1 5540CB48
P 2850 1700
F 0 "R6" H 2700 1700 50 0000 L B
F 1 "43K" H 2850 1700 50 0000 L B
	1    2850 1700
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R7
U 1 1 5540CB48
P 3200 3000
F 0 "R7" H 3050 3000 50 0000 L B
F 1 "510R" H 3200 3000 50 0000 L B
	1    3200 3000
	0    -1   -1   0   
$EndComp
$Comp
L R_R-4R* R8
U 1 1 5540CB48
P 2450 3200
F 0 "R8A" H 2300 3200 40 0000 L B
F 1 "10k" H 2450 3200 40 0000 L B
	1    2450 3200
	1    0    0    -1  
$EndComp
$Comp
L R_R-4R* R8
U 1 1 5540CB48
P 2450 3650
F 0 "R8B" H 2300 3650 40 0000 L B
F 1 "10k" H 2450 3650 40 0000 L B
	1    2450 3650
	1    0    0    -1  
$EndComp
$Comp
L R_R-4R* R8
U 1 1 5540CB48
P 1000 3200
F 0 "R8C" H 850 3200 40 0000 L B
F 1 "10k" H 1000 3200 40 0000 L B
	1    1000 3200
	-1   0    0    1   
$EndComp
$Comp
L R_R-4R* R8
U 1 1 5540CB48
P 1000 3400
F 0 "R8D" H 850 3400 40 0000 L B
F 1 "10k" H 1000 3400 40 0000 L B
	1    1000 3400
	1    0    0    -1  
$EndComp
$Comp
L R_R* R9
U 1 1 5540CB48
P 2800 3350
F 0 "R9" H 2650 3350 50 0000 L B
F 1 "DNP" H 2800 3350 50 0000 L B
	1    2800 3350
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R10
U 1 1 5540CB48
P 7400 4200
F 0 "R10" H 7250 4200 40 0000 L B
F 1 "0R" H 7450 4200 40 0000 L B
	1    7400 4200
	-1   0    0    1   
$EndComp
$Comp
L R_R* R11
U 1 1 5540CB48
P 7800 4200
F 0 "R11" H 7650 4200 40 0000 L B
F 1 "DNP" H 7850 4200 50 0000 L B
	1    7800 4200
	-1   0    0    1   
$EndComp
$Comp
L R_R* R12
U 1 1 5540CB48
P 5250 4700
F 0 "R12" H 5100 4700 40 0000 L B
F 1 "68R" H 5300 4700 40 0000 L B
	1    5250 4700
	0    1    1    0   
$EndComp
$Comp
L R_R* R13
U 1 1 5540CB48
P 6950 5100
F 0 "R13" H 6800 5100 40 0000 L B
F 1 "4.7k" H 6950 5100 40 0000 L B
	1    6950 5100
	0    1    1    0   
$EndComp
$Comp
L R_R* R14
U 1 1 5540CB48
P 7700 5100
F 0 "R14" H 7550 5100 40 0000 L B
F 1 "4.7k" H 7700 5100 40 0000 L B
	1    7700 5100
	0    1    1    0   
$EndComp
$Comp
L R_R* R15
U 1 1 5540CB48
P 6750 5400
F 0 "R15" H 6600 5400 40 0000 L B
F 1 "47k" H 6750 5400 40 0000 L B
	1    6750 5400
	0    1    1    0   
$EndComp
$Comp
L R_R* R16
U 1 1 5540CB48
P 7500 5400
F 0 "R16" H 7350 5400 40 0000 L B
F 1 "47k" H 7500 5400 40 0000 L B
	1    7500 5400
	0    1    1    0   
$EndComp
$Comp
L R_R* R17
U 1 1 5540CB48
P 1400 4850
F 0 "R17" H 1250 4850 40 0000 L B
F 1 "22R" H 1400 4850 40 0000 L B
	1    1400 4850
	-1   0    0    1   
$EndComp
$Comp
L R_R* R18
U 1 1 5540CB48
P 1400 4950
F 0 "R18" H 1250 4950 40 0000 L B
F 1 "22R" H 1400 4950 40 0000 L B
	1    1400 4950
	-1   0    0    1   
$EndComp
$Comp
L R_R* R19
U 1 1 5540CB48
P 3000 4950
F 0 "R19" H 2850 4950 40 0000 L B
F 1 "22R" H 3000 4950 40 0000 L B
	1    3000 4950
	-1   0    0    1   
$EndComp
$Comp
L R_R* R20
U 1 1 5540CB48
P 7150 5850
F 0 "R20" H 7000 5850 40 0000 L B
F 1 "300R" H 7150 5850 40 0000 L B
	1    7150 5850
	0    -1   -1   0   
$EndComp
$Comp
L R_R* R21
U 1 1 5540CB48
P 7900 5850
F 0 "R21" H 7750 5850 40 0000 L B
F 1 "300R" H 7900 5850 40 0000 L B
	1    7900 5850
	0    -1   -1   0   
$EndComp
$Comp
L SparkFun2_SIMHOLDER3 SIMHOLDER
U 1 1 5540CB48
P 2200 4850
F 0 "SIMHOLDER" H 2070 5070 40 0000 L B
F 1 "SIMHOLDER3" H 2020 4550 40 0000 L B
	1    2200 4850
	1    0    0    -1  
$EndComp
$Comp
L J_AUDIO-SMT SPK
U 1 1 5540CB48
P 6050 6400
F 0 "SPK" V 6200 6351 40 0000 L B
F 1 "" H 5300 6599 50 0000 L B
	1    6050 6400
	0    1    1    0   
$EndComp
$Comp
L Power or GND _SINGLE-PIN U$1
U 1 1 5540CB48
P 350 1350
	1    350  1350
	-1   0    0    1   
$EndComp
$Comp
L Power or GND _GND_POWER U$2
U 1 1 5540CB48
P 2550 1900
F 0 "" H 2475 1775 32 0000 L B
	1    2550 1900
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$3
U 1 1 5540CB48
P 2800 4750
F 0 "" H 2725 4625 32 0000 L B
	1    2800 4750
	0    -1   -1   0   
$EndComp
$Comp
L Power or GND _GND_POWER U$4
U 1 1 5540CB48
P 5500 6650
F 0 "" H 5425 6525 32 0000 L B
	1    5500 6650
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$5
U 1 1 5540CB48
P 6750 5750
F 0 "" H 6675 5625 32 0000 L B
	1    6750 5750
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$6
U 1 1 5540CB48
P 7500 5750
F 0 "" H 7425 5625 32 0000 L B
	1    7500 5750
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$7
U 1 1 5540CB48
P 10700 5450
F 0 "" H 10625 5325 32 0000 L B
	1    10700 5450
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$8
U 1 1 5540CB48
P 11500 1000
F 0 "" H 11425 875 32 0000 L B
	1    11500 1000
	0    -1   -1   0   
$EndComp
$Comp
L Power or GND _GND_POWER U$12
U 1 1 5540CB48
P 7450 1000
F 0 "" H 7375 875 32 0000 L B
	1    7450 1000
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$13
U 1 1 5540CB48
P 5750 4050
F 0 "" H 5675 3925 32 0000 L B
	1    5750 4050
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$14
U 1 1 5540CB48
P 4850 4700
F 0 "" H 4776 4575 32 0000 L B
	1    4850 4700
	0    1    1    0   
$EndComp
$Comp
L Power or GND _GND_POWER U$15
U 1 1 5540CB48
P 6500 4700
F 0 "" H 6425 4575 32 0000 L B
	1    6500 4700
	0    -1   -1   0   
$EndComp
$Comp
L Power or GND _GND_POWER U$16
U 1 1 5540CB48
P 4550 3250
F 0 "" H 4475 3125 32 0000 L B
	1    4550 3250
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$17
U 1 1 5540CB48
P 4500 1000
F 0 "" H 4425 875 32 0000 L B
	1    4500 1000
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$18
U 1 1 5540CB48
P 2650 450
F 0 "" H 2575 325 32 0000 L B
	1    2650 450 
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$19
U 1 1 5540CB48
P 1450 6100
F 0 "" H 1375 5975 32 0000 L B
	1    1450 6100
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$20
U 1 1 5540CB48
P 5450 4700
F 0 "" H 5376 4575 32 0000 L B
	1    5450 4700
	0    1    1    0   
$EndComp
$Comp
L Power or GND _GND_POWER U$21
U 1 1 5540CB48
P 7800 4550
F 0 "" H 7725 4425 32 0000 L B
	1    7800 4550
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _GND_POWER U$22
U 1 1 5540CB48
P 2800 3600
F 0 "" H 2725 3475 32 0000 L B
	1    2800 3600
	1    0    0    -1  
$EndComp
$Comp
L Power or GND _SINGLE-PIN U$23
U 1 1 5540CB48
P 1150 550
	1    1150 550 
	0    1    1    0   
$EndComp
$Comp
L U_MIC29302 U1
U 1 1 5540CB48
P 2550 950
F 0 "U1" H 2150 1350 40 0000 L B
F 1 "MIC29302'BU'" H 2000 1300 40 0000 L B
	1    2550 950 
	1    0    0    -1  
$EndComp
$Comp
L U_SIM900 U2
U 1 1 5540CB48
P 6400 2350
F 0 "U2" H 6100 2600 40 0000 L B
F 1 "SIM900" H 6100 2350 40 0000 L B
	1    6400 2350
	1    0    0    -1  
$EndComp
$Comp
L J_HEADER_1X2 VBAT
U 1 1 5540CB48
P 8950 5250
F 0 "VBAT" V 8780 5349 40 0000 L B
F 1 "DNP" H 8800 5750 70 0000 L B
	1    8950 5250
	0    -1   1    0   
$EndComp
$Comp
L SW_SWITCH-2CH-6P VIN
U 1 1 5540CB48
P 800 1100
F 0 "VIN" H 650 1250 40 0000 L B
F 1 "SWITCH-2CH-6P" H 650 1300 40 0000 L B
	1    800  1100
	0    -1   -1   0   
$EndComp
Wire Wire Line
7750 3200 8050 3200
Text Label 7800 3200 0 40 ~
ADC
Wire Wire Line
10650 4500 10950 4500
Text Label 10750 4500 0 40 ~
ADC
Wire Wire Line
5050 2200 4700 2200
Text Label 4700 2200 0 40 ~
CTS
Wire Wire Line
10300 1300 10550 1300
Text Label 10300 1300 0 40 ~
CTS
Wire Wire Line
9500 4150 9150 4150
Text Label 9250 4150 0 40 ~
D9
Wire Wire Line
5100 850 5100 750
Text Label 5100 700 1 40 ~
D9
Wire Wire Line
5050 2000 4700 2000
Text Label 4700 2000 0 40 ~
DCD
Wire Wire Line
10300 1100 10550 1100
Text Label 10300 1100 0 40 ~
DCD
Wire Wire Line
5050 2600 4700 2600
Text Label 4500 2600 0 40 ~
DISP_CLK
Wire Wire Line
10750 3600 11000 3600
Text Label 10700 3600 0 40 ~
DISP_CLK
Wire Wire Line
5050 2900 4700 2900
Text Label 4500 2900 0 40 ~
DISP_CS
Wire Wire Line
10750 3900 11000 3900
Text Label 10700 3900 0 40 ~
DISP_CS
Wire Wire Line
5050 2800 4700 2800
Text Label 4500 2800 0 40 ~
DISP_D/C
Wire Wire Line
10750 3800 11000 3800
Text Label 10700 3800 0 40 ~
DISP_D/C
Wire Wire Line
5050 2700 4700 2700
Text Label 4500 2700 0 40 ~
DISP_DATA
Wire Wire Line
10750 3700 11000 3700
Text Label 10700 3700 0 40 ~
DISP_DATA
Wire Wire Line
5050 2100 4700 2100
Text Label 4700 2100 0 40 ~
DSR
Wire Wire Line
10300 1200 10550 1200
Text Label 10300 1200 0 40 ~
DSR
Wire Wire Line
3550 1150 3800 1150
Wire Wire Line
3800 1150 4000 1150
Wire Wire Line
3800 1150 3800 1850
Wire Wire Line
2550 1850 2850 1850
Wire Wire Line
1650 1100 1650 1850
Wire Wire Line
1650 1850 2550 1850
Wire Wire Line
3800 1850 2850 1850
Wire Wire Line
2550 1450 2550 1850
Wire Wire Line
2550 1850 2550 1900
Wire Wire Line
1150 1100 1400 1100
Wire Wire Line
1400 1100 1400 1850
Wire Wire Line
1400 1850 1650 1850
Wire Wire Line
900 1850 1400 1850
Wire Wire Line
900 1800 900 1850
Connection ~ 3800 1150
Connection ~ 1650 1850
Connection ~ 1400 1850
Connection ~ 2550 1850
Connection ~ 2550 1900
Connection ~ 2850 1850
Connection ~ 1400 1100
Text Label 2450 2050 0 40 ~
GND
Wire Wire Line
5850 950 5950 950
Wire Wire Line
5950 950 6050 950
Wire Wire Line
6050 950 6150 950
Wire Wire Line
6150 950 6250 950
Wire Wire Line
6250 950 6350 950
Wire Wire Line
6350 950 6450 950
Wire Wire Line
6450 950 6550 950
Wire Wire Line
6550 950 6650 950
Wire Wire Line
6650 950 6750 950
Wire Wire Line
6750 950 6850 950
Wire Wire Line
6850 950 6950 950
Wire Wire Line
6950 950 7050 950
Wire Wire Line
7050 950 7150 950
Wire Wire Line
7150 950 7250 950
Wire Wire Line
7250 950 7450 950
Wire Wire Line
7450 950 7450 1000
Connection ~ 5950 950
Connection ~ 6050 950
Connection ~ 6150 950
Connection ~ 6250 950
Connection ~ 6350 950
Connection ~ 6450 950
Connection ~ 6550 950
Connection ~ 6650 950
Connection ~ 6750 950
Connection ~ 6850 950
Connection ~ 6950 950
Connection ~ 7050 950
Connection ~ 7150 950
Connection ~ 7250 950
Text Label 7350 950 0 40 ~
GND
Wire Wire Line
2450 450 2650 450
Wire Wire Line
2150 450 2450 450
Connection ~ 2450 450
Text Label 2700 550 0 40 ~
GND
Wire Wire Line
7150 4500 7600 4500
Wire Wire Line
7600 4500 7800 4500
Wire Wire Line
7800 4500 8450 4500
Wire Wire Line
8450 4200 8450 4500
Wire Wire Line
7800 4550 7800 4500
Wire Wire Line
8450 4200 8450 3900
Wire Wire Line
8450 3900 8450 3800
Wire Wire Line
8450 3800 8450 3700
Connection ~ 7600 4500
Connection ~ 7800 4500
Connection ~ 8450 4200
Connection ~ 8450 3900
Connection ~ 8450 3800
Text Label 7750 4700 0 40 ~
GND
Wire Wire Line
1650 4450 2700 4450
Wire Wire Line
2700 4450 2700 4750
Wire Wire Line
2800 4750 2700 4750
Connection ~ 2700 4750
Text Label 2950 4800 1 40 ~
GND
Wire Wire Line
6750 5550 6750 5600
Wire Wire Line
6750 5600 6750 5750
Connection ~ 6750 5600
Text Label 6650 5950 0 40 ~
GND
Wire Wire Line
4550 3200 4550 3250
Text Label 4600 3350 0 40 ~
GND
Wire Wire Line
6500 4550 6500 4700
Wire Wire Line
6500 4700 6500 4850
Connection ~ 6500 4700
Text Label 6550 4800 1 40 ~
GND
Wire Wire Line
7500 5550 7500 5600
Wire Wire Line
7500 5600 7500 5750
Connection ~ 7500 5600
Text Label 7450 5900 0 40 ~
GND
Wire Wire Line
4900 4850 4900 4700
Wire Wire Line
4850 4700 4900 4700
Wire Wire Line
4900 4700 4900 4550
Connection ~ 4900 4700
Text Label 4750 4800 1 40 ~
GND
Wire Wire Line
9350 5300 9100 5300
Text Label 9350 5250 0 40 ~
GND
Wire Wire Line
10650 4400 10950 4400
Text Label 10750 4400 0 40 ~
GND
Wire Wire Line
5750 4050 5750 3750
Text Label 5800 4150 0 40 ~
GND
Wire Wire Line
5650 6600 6450 6600
Wire Wire Line
4500 6300 4500 6600
Wire Wire Line
4500 6600 5300 6600
Wire Wire Line
5300 6600 5300 6200
Wire Wire Line
5650 6600 5500 6600
Wire Wire Line
5500 6650 5500 6600
Wire Wire Line
5500 6600 5300 6600
Wire Wire Line
6450 6200 6450 6600
Wire Wire Line
5650 6300 5650 6600
Connection ~ 5300 6600
Connection ~ 5650 6600
Connection ~ 5500 6600
Text Label 5600 6750 0 40 ~
GND
Wire Wire Line
4600 950 4500 950
Wire Wire Line
4500 950 4500 1000
Wire Wire Line
4600 1100 4600 950
Wire Wire Line
4600 950 4800 950
Connection ~ 4600 950
Text Label 4550 1100 0 40 ~
GND
Wire Wire Line
11250 1000 11500 1000
Text Label 11300 1000 0 40 ~
GND
Wire Wire Line
1700 6100 1450 6100
Text Label 1200 6050 0 40 ~
GND
Wire Wire Line
5450 4700 5500 4700
Wire Wire Line
5500 4850 5500 4700
Wire Wire Line
5500 4700 5500 4550
Connection ~ 5500 4700
Text Label 5450 4950 1 40 ~
GND
Wire Wire Line
2800 3600 2800 3550
Wire Wire Line
2800 3550 2800 3500
Wire Wire Line
2800 3550 3000 3550
Wire Wire Line
3000 3550 3000 3500
Wire Wire Line
3000 3550 3200 3550
Wire Wire Line
3200 3550 3200 3500
Connection ~ 2800 3550
Connection ~ 3000 3550
Wire Wire Line
9500 2100 9150 2100
Text Label 9250 2100 0 40 ~
GND
Wire Wire Line
9500 2200 9150 2200
Text Label 9250 2200 0 40 ~
GND
Wire Wire Line
9500 4650 9150 4650
Text Label 9250 4650 0 40 ~
GND
Wire Wire Line
10850 5400 10700 5400
Wire Wire Line
10700 5400 10700 5450
Wire Wire Line
7750 2700 8050 2700
Text Label 7800 2700 0 40 ~
GPIO1
Wire Wire Line
10600 1750 10300 1750
Text Label 10300 1750 0 40 ~
GPIO1
Wire Wire Line
7750 2600 8050 2600
Text Label 7800 2600 0 40 ~
GPIO2
Wire Wire Line
11200 1750 11500 1750
Text Label 11200 1750 0 40 ~
GPIO2
Wire Wire Line
7750 2500 8050 2500
Text Label 7800 2500 0 40 ~
GPIO3
Wire Wire Line
10600 1850 10300 1850
Text Label 10300 1850 0 40 ~
GPIO3
Wire Wire Line
7750 2400 8050 2400
Text Label 7800 2400 0 40 ~
GPIO4
Wire Wire Line
11200 1850 11500 1850
Text Label 11200 1850 0 40 ~
GPIO4
Wire Wire Line
7750 2300 8050 2300
Text Label 7800 2300 0 40 ~
GPIO5
Wire Wire Line
10600 1950 10300 1950
Text Label 10300 1950 0 40 ~
GPIO5
Wire Wire Line
7750 2200 8050 2200
Text Label 7800 2200 0 40 ~
GPIO6
Wire Wire Line
11200 1950 11500 1950
Text Label 11200 1950 0 40 ~
GPIO6
Wire Wire Line
7750 2100 8050 2100
Text Label 7800 2100 0 40 ~
GPIO7
Wire Wire Line
10600 2050 10300 2050
Text Label 10300 2050 0 40 ~
GPIO7
Wire Wire Line
7750 2000 8050 2000
Text Label 7800 2000 0 40 ~
GPIO8
Wire Wire Line
11200 2050 11500 2050
Text Label 11200 2050 0 40 ~
GPIO8
Wire Wire Line
7750 1900 8050 1900
Text Label 7800 1900 0 40 ~
GPIO9
Wire Wire Line
10600 2150 10300 2150
Text Label 10300 2150 0 40 ~
GPIO9
Wire Wire Line
7750 1800 8050 1800
Text Label 7800 1800 0 40 ~
GPIO10
Wire Wire Line
11200 2150 11500 2150
Text Label 11200 2150 0 40 ~
GPIO10
Wire Wire Line
7750 1700 8050 1700
Text Label 7800 1700 0 40 ~
GPIO11
Wire Wire Line
10600 2250 10300 2250
Text Label 10300 2250 0 40 ~
GPIO11
Wire Wire Line
7750 1600 8050 1600
Text Label 7800 1600 0 40 ~
GPIO12
Wire Wire Line
11200 2250 11500 2250
Text Label 11200 2250 0 40 ~
GPIO12
Wire Wire Line
2100 3200 2300 3200
Wire Wire Line
2100 3250 2100 3200
Connection ~ 2100 3200
Text Label 2050 3150 0 40 ~
HRXD
Wire Wire Line
10600 2900 10350 2900
Text Label 10350 2900 0 40 ~
HRXD
Wire Wire Line
2100 3650 2300 3650
Wire Wire Line
2100 3700 2100 3650
Connection ~ 2100 3650
Text Label 2050 3600 0 40 ~
HTXD
Wire Wire Line
11200 2900 11500 2900
Text Label 11250 2900 0 40 ~
HTXD
Wire Wire Line
6350 3750 6350 4250
Text Label 6350 4250 1 40 ~
LDBG_RX
Wire Wire Line
6250 3750 6250 4250
Text Label 6250 4250 1 40 ~
LDBG_TX
Wire Wire Line
5050 1800 4700 1800
Text Label 4700 1800 0 40 ~
LDTR
Wire Wire Line
11250 1400 11500 1400
Text Label 11250 1400 0 40 ~
LDTR
Wire Wire Line
6150 3750 6150 4250
Text Label 6150 4250 1 40 ~
LINEIN_L
Wire Wire Line
11200 4300 11400 4300
Text Label 11200 4400 0 40 ~
LINEIN_L
Wire Wire Line
6050 3750 6050 4250
Text Label 6050 4250 1 40 ~
LINEIN_R
Wire Wire Line
11200 4200 11400 4200
Text Label 11200 4100 0 40 ~
LINEIN_R
Wire Wire Line
5050 2300 4700 2300
Text Label 4700 2300 0 40 ~
LRTS
Wire Wire Line
10300 1400 10550 1400
Text Label 10300 1400 0 40 ~
LRTS
Wire Wire Line
5050 2500 4700 2500
Text Label 4700 2500 0 40 ~
LRXD
Wire Wire Line
1150 3200 1700 3200
Text Label 1300 3200 0 40 ~
LRXD
Wire Wire Line
11250 1300 11500 1300
Text Label 11250 1300 0 40 ~
LRXD
Wire Wire Line
5050 2400 4700 2400
Text Label 4700 2400 0 40 ~
LTXD
Wire Wire Line
850 3400 750 3400
Wire Wire Line
750 3400 750 3650
Wire Wire Line
750 3650 1700 3650
Text Label 1300 3650 0 40 ~
LTXD
Wire Wire Line
11250 1200 11500 1200
Text Label 11250 1200 0 40 ~
LTXD
Wire Wire Line
9150 3000 9500 3000
Text Label 9200 3000 0 40 ~
MRXD
Wire Wire Line
10600 2800 10350 2800
Text Label 10350 2800 0 40 ~
MRXD
Wire Wire Line
9150 2900 9500 2900
Text Label 9200 2900 0 40 ~
MTXD
Wire Wire Line
11200 2800 11500 2800
Text Label 11250 2800 0 40 ~
MTXD
Wire Wire Line
7250 4200 7150 4200
Wire Wire Line
7150 4200 7150 3750
Connection ~ 7150 4200
Wire Wire Line
2850 1500 2850 1550
Wire Wire Line
2850 1500 3350 1500
Wire Wire Line
2850 1450 2850 1500
Wire Wire Line
3350 1150 3350 1500
Connection ~ 2850 1500
Wire Wire Line
7150 5700 7150 5600
Wire Wire Line
2700 4950 2850 4950
Wire Wire Line
1700 4950 1550 4950
Wire Wire Line
1700 4850 1550 4850
Wire Wire Line
7150 6100 7150 6000
Wire Wire Line
6950 5250 6950 5400
Wire Wire Line
6950 5250 6750 5250
Connection ~ 6950 5250
Wire Wire Line
2600 3200 2650 3200
Wire Wire Line
2650 3200 2650 3650
Wire Wire Line
2650 3650 2600 3650
Wire Wire Line
2650 3200 2800 3200
Wire Wire Line
2800 3200 3000 3200
Wire Wire Line
3000 3200 3200 3200
Wire Wire Line
3200 3200 3200 3150
Connection ~ 2650 3200
Connection ~ 2800 3200
Connection ~ 3000 3200
Connection ~ 3200 3200
Wire Wire Line
6950 4950 6950 3750
Wire Wire Line
4350 2300 4600 2300
Wire Wire Line
4600 2300 4600 1700
Wire Wire Line
4600 1700 5050 1700
Wire Wire Line
10650 5000 10700 5000
Wire Wire Line
10700 5000 10700 5100
Wire Wire Line
10700 5100 10700 5200
Wire Wire Line
10700 5200 10700 5300
Wire Wire Line
10650 5300 10700 5300
Wire Wire Line
10650 5200 10700 5200
Wire Wire Line
10650 5100 10700 5100
Wire Wire Line
10700 5300 10850 5300
Connection ~ 10700 5200
Connection ~ 10700 5100
Connection ~ 10700 5300
Wire Wire Line
5250 4550 5200 4550
Wire Wire Line
5250 4550 5250 4450
Wire Wire Line
5250 4450 5650 4450
Wire Wire Line
5650 4450 5650 3750
Connection ~ 5250 4550
Wire Wire Line
5100 1300 5100 1250
Wire Wire Line
5800 4850 5850 4850
Wire Wire Line
5850 4550 5800 4550
Wire Wire Line
5850 4850 5850 4550
Wire Wire Line
5850 4550 5850 3750
Wire Wire Line
5850 4850 5850 5100
Wire Wire Line
5850 5100 5600 5100
Wire Wire Line
5600 5100 5600 5600
Wire Wire Line
5600 5600 5650 5600
Connection ~ 5850 4850
Connection ~ 5850 4550
Wire Wire Line
7900 5700 7900 5600
Wire Wire Line
7900 6050 7900 6000
Wire Wire Line
7700 5250 7700 5400
Wire Wire Line
7700 5250 7500 5250
Connection ~ 7700 5250
Wire Wire Line
6200 4550 6150 4550
Wire Wire Line
6150 4850 6150 4550
Wire Wire Line
6200 4850 6150 4850
Wire Wire Line
6150 4550 6150 4450
Wire Wire Line
5950 3750 5950 4450
Wire Wire Line
6150 4450 5950 4450
Wire Wire Line
6500 6000 6500 5100
Wire Wire Line
6500 5100 6150 5100
Wire Wire Line
6150 5100 6150 4850
Wire Wire Line
6500 6000 6450 6000
Connection ~ 6150 4550
Connection ~ 6150 4850
Wire Wire Line
7050 3750 7050 4750
Wire Wire Line
7050 4750 7700 4750
Wire Wire Line
7700 4750 7700 4950
Wire Wire Line
7650 4200 7600 4200
Wire Wire Line
7600 4200 7550 4200
Wire Wire Line
7850 3800 7600 3800
Wire Wire Line
7600 3800 7600 4200
Connection ~ 7600 4200
Wire Wire Line
10650 4200 10900 4200
Wire Wire Line
9150 3100 9500 3100
Wire Wire Line
9150 3200 9500 3200
Wire Wire Line
9150 3300 9500 3300
Wire Wire Line
9150 3400 9500 3400
Wire Wire Line
9150 3500 9500 3500
Wire Wire Line
10650 4300 10900 4300
Wire Wire Line
5200 4850 5250 4850
Wire Wire Line
5300 6000 5350 6000
Wire Wire Line
5350 6000 5350 5100
Wire Wire Line
5350 5100 5250 5100
Wire Wire Line
5250 5100 4450 5100
Wire Wire Line
4450 5100 4450 5600
Wire Wire Line
4450 5600 4500 5600
Wire Wire Line
5250 4850 5250 5100
Connection ~ 5250 4850
Connection ~ 5250 5100
Wire Wire Line
9500 4250 9150 4250
Wire Wire Line
9500 4350 9150 4350
Wire Wire Line
9500 4450 9150 4450
Wire Wire Line
9500 4550 9150 4550
Wire Wire Line
4800 1300 4800 1250
Connection ~ 4800 1300
Wire Wire Line
9500 4750 9150 4750
Wire Wire Line
9500 1200 9150 1200
Wire Wire Line
9500 1300 9150 1300
Wire Wire Line
9500 1400 9150 1400
Wire Wire Line
9500 1500 9150 1500
Wire Wire Line
9500 2000 9150 2000
Wire Wire Line
9500 2400 9150 2400
Wire Wire Line
9500 2500 9150 2500
Wire Wire Line
1850 450 1750 450
Wire Wire Line
7750 2900 8050 2900
Text Label 7800 2900 0 40 ~
PWM1
Wire Wire Line
10650 4700 10950 4700
Text Label 10700 4700 0 40 ~
PWM1
Wire Wire Line
7750 2800 8050 2800
Text Label 7800 2800 0 40 ~
PWM2
Wire Wire Line
10650 4600 10950 4600
Text Label 10700 4600 0 40 ~
PWM2
Wire Wire Line
4600 1500 4600 1600
Wire Wire Line
4600 1600 5050 1600
Wire Wire Line
4350 1500 4600 1500
Connection ~ 4600 1500
Text Label 4700 1600 0 40 ~
PWRKEY
Wire Wire Line
5050 1900 4700 1900
Text Label 4700 1900 0 40 ~
RI
Wire Wire Line
10300 1000 10550 1000
Text Label 10300 1000 0 40 ~
RI
Wire Wire Line
7750 3100 8050 3100
Text Label 7800 3100 0 40 ~
SCL
Wire Wire Line
9500 1000 9150 1000
Text Label 9250 1000 0 40 ~
SCL
Wire Wire Line
7750 3000 8050 3000
Text Label 7800 3000 0 40 ~
SDA
Wire Wire Line
9500 1100 9150 1100
Text Label 9250 1100 0 40 ~
SDA
Wire Wire Line
6650 3750 6650 4250
Text Label 6650 4250 1 40 ~
SIM_CLK
Wire Wire Line
1250 4950 800 4950
Text Label 800 4950 0 40 ~
SIM_CLK
Wire Wire Line
6550 3750 6550 4250
Text Label 6550 4250 1 40 ~
SIM_DATA
Wire Wire Line
3150 4950 3500 4950
Text Label 3200 4950 0 40 ~
SIM_DATA
Wire Wire Line
6750 3750 6750 4250
Text Label 6750 4250 1 40 ~
SIM_RST
Wire Wire Line
1250 4850 800 4850
Text Label 800 4850 0 40 ~
SIM_RST
Wire Wire Line
6450 3750 6450 4250
Text Label 6450 4250 1 40 ~
SIM_VDD
Wire Wire Line
1700 4750 1650 4750
Wire Wire Line
1650 4750 800 4750
Connection ~ 1650 4750
Text Label 800 4750 0 40 ~
SIM_VDD
Wire Wire Line
10600 3000 10350 3000
Text Label 10350 3000 0 40 ~
SRXD
Wire Wire Line
9500 4050 9150 4050
Text Label 9200 4050 0 40 ~
SRXD
Wire Wire Line
11200 3000 11500 3000
Text Label 11250 3000 0 40 ~
STXD
Wire Wire Line
9150 3600 9500 3600
Text Label 9200 3600 0 40 ~
STXD
Wire Wire Line
3550 850 3550 800
Wire Wire Line
3550 800 3550 400
Wire Wire Line
3350 800 3350 850
Wire Wire Line
3200 800 3350 800
Wire Wire Line
3350 800 3550 800
Wire Wire Line
3550 800 3800 800
Wire Wire Line
3800 800 3800 850
Wire Wire Line
3800 800 4000 800
Wire Wire Line
4000 800 4000 850
Connection ~ 3350 800
Connection ~ 3550 800
Connection ~ 3800 800
Text Label 3550 350 1 40 ~
VBAT
Wire Wire Line
5750 950 5650 950
Wire Wire Line
5650 950 5550 950
Wire Wire Line
5550 950 5550 750
Connection ~ 5650 950
Connection ~ 5550 950
Text Label 5550 700 0 40 ~
VBAT
Wire Wire Line
9350 5200 9100 5200
Text Label 9350 5150 0 40 ~
VBAT
Wire Wire Line
9500 2300 9150 2300
Text Label 9150 2300 0 40 ~
VBOARD
Wire Wire Line
700 1350 700 850
Wire Wire Line
700 1350 450 1350
Connection ~ 700 1350
Text Label 250 1450 0 40 ~
VBOARD
Wire Wire Line
5050 3000 4700 3000
Text Label 4500 3000 0 40 ~
VDD_EXT
Wire Wire Line
1900 3000 1900 2900
Wire Wire Line
1900 2900 750 2900
Wire Wire Line
750 2900 750 3200
Wire Wire Line
850 3200 750 3200
Text Label 1300 2900 0 40 ~
VDD_EXT
Wire Wire Line
1900 3450 1900 3400
Wire Wire Line
1900 3400 1150 3400
Text Label 1300 3400 0 40 ~
VDD_EXT
Wire Wire Line
11250 1100 11500 1100
Text Label 11250 1100 0 40 ~
VDD_EXT
Wire Wire Line
900 1600 900 1350
Wire Wire Line
900 850 900 1350
Connection ~ 900 1350
Text Label 950 1600 0 40 ~
VEXT
Wire Wire Line
1650 800 1850 800
Wire Wire Line
2250 1450 1850 1450
Wire Wire Line
1850 1450 1850 800
Wire Wire Line
1650 800 1400 800
Wire Wire Line
1400 800 1150 800
Wire Wire Line
1900 800 1850 800
Wire Wire Line
800 850 800 800
Wire Wire Line
800 800 1150 800
Wire Wire Line
800 1350 800 850
Wire Wire Line
1450 450 1400 450
Wire Wire Line
1400 450 1400 800
Wire Wire Line
1150 800 1150 600
Connection ~ 1650 800
Connection ~ 1850 800
Connection ~ 1400 800
Connection ~ 1150 800
Connection ~ 800 850
Text Label 1250 350 0 40 ~
VIN
Wire Wire Line
7900 6350 7900 6600
Text Label 7900 6650 1 40 ~
VIN
Wire Wire Line
7150 6400 7150 6600
Text Label 7150 6650 1 40 ~
VIN
Wire Wire Line
3200 2850 3200 2750
Text Label 3150 2700 0 70 ~
VIN
Wire Wire Line
4850 3200 5050 3200
Text Label 4850 3200 0 40 ~
VRTC
Wire Wire Line
2400 6100 3000 6100
Text Label 2800 6100 0 40 ~
VRTC
Wire Notes Line
4500 4300 6650 4300
Wire Notes Line
6650 4300 6650 5050
Wire Notes Line
6650 5050 4500 5050
Wire Notes Line
4500 5050 4500 4300
Wire Notes Line
7100 4250 7700 4250
Wire Notes Line
7700 4250 7700 4600
Wire Notes Line
7700 4600 7100 4600
Wire Notes Line
7100 4600 7100 4250
Wire Notes Line
8405 8151 9980 8151
Wire Notes Line
9980 8151 10925 8151
Wire Notes Line
10925 8151 11594 8151
Wire Notes Line
11594 8151 11594 7875
Wire Notes Line
11594 7875 11594 7557
Wire Notes Line
11594 7557 11594 7167
Wire Notes Line
11594 7167 11594 6655
Wire Notes Line
8405 8151 8405 7875
Wire Notes Line
8405 7875 8405 7557
Wire Notes Line
8405 7557 8405 7167
Wire Notes Line
8405 7167 8405 6655
Wire Notes Line
8405 6655 11594 6655
Wire Notes Line
8405 7167 11594 7167
Wire Notes Line
8405 7875 11594 7875
Wire Notes Line
8405 7557 11594 7557
Wire Notes Line
9980 7561 9980 8151
Wire Notes Line
10925 7875 10925 8151
Wire Notes Line
4100 93 4100 2450
Wire Notes Line
4100 2450 98 2450
Wire Notes Line
4100 2450 4100 4100
Wire Notes Line
4100 4100 98 4100
Wire Notes Line
4100 4100 4100 5400
Wire Notes Line
4100 5400 98 5400
Wire Notes Line
4100 5400 4100 6550
Wire Notes Line
4100 6550 98 6550
Wire Notes Line
98 80 98 2450
Wire Notes Line
98 2450 98 4100
Wire Notes Line
98 4100 98 5400
Wire Notes Line
98 5400 98 6550
Wire Notes Line
98 6550 98 8151
Wire Notes Line
98 8151 8464 8151
Wire Notes Line
98 80 11594 80
Wire Notes Line
11594 80 11594 6714
Wire Notes Line
8405 7875 6259 7875
Wire Notes Line
6259 7875 6259 8151
Wire Notes Line
8405 7600 6259 7600
Wire Notes Line
6259 7600 6259 7875
Wire Notes Line
8600 150 8600 6500
$EndSCHEMATC
